org 7e00h
bits 16


jmp _second_start

%include "colour.nasm"

introstring: db "Success in loading the second stage", NEWL, 0

greeting: db col(DRED), 'S'
          db col(LRED), 'p' 
          db col(LYEL), 'a' 
          db col(LGRN), 'c' 
          db col(DGRN), 'e'
                     db ' ' 
          db col(LTEL), 'D' 
          db col(DTEL), 'r' 
          db col(LBLU), 'i' 
          db col(DPNK), 'f' 
          db col(LPNK), 't' 
          db col(DRED), 'e' 
          db col(LRED), 'r' 
          db NEWL, 0

credits: db "Written by Alex O'Leary for ", col(LYEL), "Ludum", NEWL
times 31 db ' ' 
         db col(DYEL), "Dare", NEWL 
times 34 db ' ' 
         db col(LRED), "42!", NEWL,     \
            col(LWHT), "(twitter: ",    \
            col(LBLU), "@gallefray",    \
            col(LWHT), " | website: ",  \
            col(LGRN), "nuzz.nz",       \
            col(LWHT), ")", NEWL, NEWL, 0

information: db col(LRED), "This is it! ", col(LWHT), "The ", col(DRED), "big crunch ", \
                col(LWHT), "has ", col(LRED), "arrived", col(LWHT), "!", NEWL, NEWL 
   db col(LWHT), "Good news! There is a safe place at the center of the Universe", NEWL
   db "You must now get there, before you get ", col(DYEL), "crushed!", NEWL, NEWL
   db col(LYEL), "To use thrusters, use the ", col(LBLU), "arrow keys", NEWL
   db col(LYEL), "To speed towards the ", col(DYEL), "highlighted sun", \
      col(LYEL), " press ", col(LBLU), "x", NEWL, NEWL
   db col(LPNK), "Good luck ", col(LRED), "Cosmonaut!", NEWL, 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; set_pixel(al=colour, cx=x, dx=y) C(ah,bh)
set_pixel:
	mov ah, 0Ch
	xor bh, bh
	int 10h
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; memset(di=dest, al=char, cx=length)
memset:
	cld
.loop:
	stosb
	loop .loop
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; memcpy(si=src, di=dest, cx=length)
memcpy:
	cld
.loop:
	movsb
	loop .loop
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; scrnblit() C(si, di, ax, bx, cx, dx)
scrnblit:
	xor dx, dx  ; y
	xor bx, bx
	mov si, [scrnbuf]
.next_line:
	mov cx, 300 ; x
.blit:
	lodsb       ; al = *si++
	test al, al
	je .end

	mov ah, 0Ch
	dec al
	int 10h
	loop .blit
.check_y:
	inc dx
	cmp dx, 200
	jl .next_line
	; otherwise y == 300, so we finish and clean the buffer
.end:
	mov di, scrnbuf
	mov al, LBLK+1
	mov cx, (300*200)
	call memset
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; colputs(si=string) C(ax, bx)   Color=[1, #col, ...]
colputs:
	mov bl, LWHT
.loop:
	lodsb
	test al, al
	je .null
	cmp al, CSNT
	je .sentinel
	
	; putc
	mov ah, 0Eh
	xor bh, bh
	int 10h

	jmp .loop
.sentinel:
	lodsb
	mov bl, al
	jmp .loop
.null:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; putrect(si=rect, dx=y, cx=x) C(di)
putrect: ; (si=rect, dx=y, cx=x) C(di)
	push cx
	mov [.origx], cx
	add cx, word [si]
	mov [.limit], cx
	pop cx
	add si, 2 ; skip over word
.loop:
	lodsb
	test al, al
	je .null
	dec al
	call set_pixel
	inc cx               ; x = x + 1

	mov ax, word [.limit]
	cmp cx, ax
	je .next_line

	jmp .loop
.next_line:
	mov cx, word [.origx]
	inc dx
	jmp .loop
.null:
	ret
.origx: dw 0
.limit: dw 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; getkey() => al=char C(ax)
getkey:
	xor ax, ax
	int 16h
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; setcur(dh=y, dl=x) C(ah, bh, dx)
setcur: 
	mov ah, 02h 
	xor bh, bh 
	int 10h 
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; clear_screen() C(ah)
clear_screen:
	xor dx, dx
	call setcur

	mov si, .line
	call colputs
	ret
.line: db col(LRED)
       times 600 db ' '
       db 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; check_key() C(ax) => ZF and al if key
check_key:
	mov ah, 01h
	call 16h
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PLAYER

player_x:  dw ((300/2)-(player_w/2))
player_y:  dw ((200/2)-(player_h/2))
player_dx: dw 0
player_dy: dw 0
player_w equ 32
player_h equ 32

player_thrust equ 4

player_model: dw 32
times (32*32) db LRED+1
              db 0

move_left: ; C(ax)
	mov ax, [player_dx]
	sub ax, 2
	mov [player_dx], ax
	ret
move_right: ; C(ax)
	mov ax, [player_dx]
	add ax, 2
	mov [player_dx], ax
	ret
apply_thrust: ; C(ax)
	mov ax, [player_x]
	add ax, [player_dx]
	mov [player_x], ax

	mov ax, [player_y]
	add ax, [player_dy]
	mov [player_y], ax

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; mainloop
pkey: db col(LWHT), "key: "
key:  db ' ', NEWL, 0

mainloop:
	mov si, pkey
	call colputs 
	call getkey

	mov di, scrnbuf
	mov al, LBLK+1
	mov cx, 300*200
	call memset

	mov si, _second_start.press_enter
	call colputs

	call getkey
.loop:
	xor cx, cx
	xor dx, dx
	call setcur

	mov di, scrnbuf
	add di, 10
	mov al, LRED+1
	mov cx, 10
	call memset

	call scrnblit
	jmp .loop
.end:
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; _second_start()
_second_start: 
	call clear_screen 

	xor dx, dx
	call setcur
	mov si, greeting
	call colputs
	mov si, credits
	call colputs 
	mov si, .press_enter
	call colputs
	call getkey

	call clear_screen

	xor dx, dx
	call setcur
	mov si, information
	call colputs

	call getkey
	call clear_screen

	call mainloop

	jmp $

.press_enter: db col(LWHT), "Press ", col(LGRN), "enter", col(LWHT), " to continue!", NEWL, 0
scrnbuf:

