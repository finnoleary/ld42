BIN= boot.bin
GBIN= game.bin
OBIN= out.bin
DISK= disk.fd

build:QV: $DISK
 :

$BIN:V:
  nasm -f bin boot.nasm -o $BIN
  nasm -f bin game.nasm -o $GBIN
  cat $BIN $GBIN > $OBIN

$DISK:V: $BIN
  # dd if=/dev/zero of=$DISK bs=1509949 count=1
  [ -f $DISK ] && rm $DISK
  mkdosfs -C $DISK 1440
  dd if=$OBIN of=$DISK conv=notrunc

server:V: $DISK
  qemu-system-i386 -fda $DISK

client:V: $DISK
  gvncviewer 127.0.0.1

clean:V:
  rm -f $BIN $GBIN $OBIN $DISK
