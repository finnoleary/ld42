%define CSNT 0x01 ; colour sentinel
%define CTRP 0xFE ; colour transparent
%define col(c) CSNT, c

%define DBLK 0x00 ; dark black
%define LBLK 0x08 ; dark grey
%define DBLU 0x01 ; dark blue
%define LBLU 0x09 ; light blue
%define DGRN 0x02 ; dark green
%define LGRN 0x0A ; light green
%define DTEL 0x03 ; dark teal
%define LTEL 0x0B ; light teal
%define DRED 0x04 ; dark red
%define LRED 0x0C ; light red
%define DPNK 0x05 ; dark pink
%define LPNK 0x0D ; light pink
%define DYEL 0x06 ; orange
%define LYEL 0x0E ; light yellow
%define DWHT 0x07 ; grey
%define LWHT 0x0F ; white

%define NEWL 10, 13 
