%include "colour.nasm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; load_graphics() C(ax)
load_graphics:
	mov ah, 00h
	mov al, 13h
	int 10h
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; set_pixel(al=colour, cx=x, dx=y) C(ah,bh)
set_pixel:
	mov ah, 0Ch
	xor bh, bh
	int 10h
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; putc(al=char, bl=colour) C(ah,bh)
putc:
	mov ah, 0Eh
	xor bh, bh
	int 10h
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; puts(si=string, dl=colour) C(ah,bh)
puts:
	lodsb
	test al, al
	je .null
	mov bl, dl
	call putc
	jmp puts
.null:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; colputs(si=string) C(ax, bx)   Color=[1, #col, ...]
colputs:
	mov bl, LWHT
.loop:
	lodsb
	test al, al
	je .null
	cmp al, CSNT
	je .sentinel
	
	call putc
	jmp .loop
.sentinel:
	lodsb
	mov bl, al
	jmp .loop
.null:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; putrect(si=rect, dx=y, cx=x) C(di)
putrect: ; (si=rect, dx=y, cx=x) C(di)
	push cx
	mov [.origx], cx
	add cx, word [si]
	mov [.limit], cx
	pop cx
	add si, 2 ; skip over word
.loop:
	lodsb
	test al, al
	je .null
	dec al
	call set_pixel
	inc cx               ; x = x + 1

	mov ax, word [.limit]
	cmp cx, ax
	je .next_line

	jmp .loop
.next_line:
	mov cx, word [.origx]
	inc dx
	jmp .loop
.null:
	ret
.origx: dw 0
.limit: dw 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; getkey() => al=char C(ax)
getkey:
	xor ax, ax
	int 16h
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; reboot()
reboot:
	xor ax, ax
	int 19h

