bits 16
ORG 7c00h
head equ $
jmp _start

%include "basedraw.nasm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; reset_drive() C(ah, dl) => ah (0=success)
reset_drive:
	xor ah, ah
	mov dl, [bootdevice]
	int 13h


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; get_drive_data()
;get_drive_data:
;	mov dl, [bootdevice]
;	mov ah, 08h
;	int 13h
;	jc .error
;.success:
;	and cx, 3Fh
;	mov [sectors_per_track], cx
;	mov ah, 0
;	ret
;.error:
;	mov ah, 1
;	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; load_block(al=nsectors, ch=cylinder, cl=sector, dh=head, es:bx=buffer)
load_block:
	mov ah, 02h
	; mov dl, [bootdevice]
	mov dl, 00h
	mov [.axback], ax
	mov [.cxback], cx
	mov [.dxback], dx
.loop:
	mov bx, 7e00h
	int 13h
	jc .end
	test ah, ah
	je .end
.retry_error:
	mov si, .resetd
	mov dl, LRED
	call puts

	call reset_drive
	jc .fatal 

	mov ax, 0000h
	mov es, ax
	mov bx, 7e00h

	mov ax, [.axback]
	mov cx, [.cxback]
	mov dx, [.dxback]
	jmp .loop
.end:
	mov si, .str
	mov dl, LGRN
	call puts
	mov ah, 0
	ret
.fatal:
	mov ah, 1
	ret
.axback: dw 0
.cxback: dw 0
.dxback: dw 0
.str: db "Loaded successfully", NEWL, 0
.resetd: db "Error on read, resetting and retrying...", NEWL, 0
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; _start
bootdevice: db 0

_start:
	mov [bootdevice], dl
	call load_graphics

	mov dl, LWHT
	mov si, .string
	call puts

	call getkey

	call reset_drive
	test ah, ah
	jne .error

	mov al, 1Fh
	mov ch, 00h ; cylinder
	mov cl, 02h ; cylinder
	mov dh, 00h ; head

	call load_block
	test ah, ah
	jne .error

	mov si, .strstr
	call puts

	jmp 0000h:7e00h

.error:
	mov si, .errstr
	call puts
	
	call getkey

	nop
	jmp $

.string: db "Loaded first stage. Press ENTER to load second stage!", NEWL, 0
.strstr: db "Jumping to second stage...", NEWL, 0
.errstr: db "Failure in loading second stage. Unable to continue", NEWL, 0

times 510-($-$$) db 0
db 0x55
db 0xAA
